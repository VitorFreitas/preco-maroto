<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Simple CRUD Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    
    <link href="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
</head>

<body>
    <div class="page-header">
        <h1>Simple CRUD Page</h1>
    </div>
 
 	<h1> Person : ${person.firstName}</h1>
 	
 	<script src="http://code.jquery.com/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/assets/bootstrap/css/bootstrap.min.css"></script>
 </body>
</html>