package com.example.model;

import org.springframework.data.neo4j.annotation.GraphId
import org.springframework.data.neo4j.annotation.NodeEntity

@NodeEntity
class Person {

	@GraphId Integer id

    String firstName

    String lastName

}
