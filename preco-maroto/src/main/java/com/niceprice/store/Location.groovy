package com.niceprice.store

import org.springframework.data.neo4j.annotation.NodeEntity;


@NodeEntity
class Location {

	String street

	String number

}
