package com.niceprice.store

import org.springframework.data.neo4j.annotation.NodeEntity;

import com.niceprice.product.Product


@NodeEntity
class StoreProduct {

	BigDecimal price

}
