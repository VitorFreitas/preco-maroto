package com.niceprice.product

import org.springframework.data.neo4j.annotation.NodeEntity;

@NodeEntity
class Manufacturer {

	String name

	String link
	
}
