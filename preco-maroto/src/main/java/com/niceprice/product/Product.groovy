package com.niceprice.product

import org.springframework.data.neo4j.annotation.NodeEntity;


@NodeEntity
class Product {

	String productId

	String name

	String description

	String imgUrl

}