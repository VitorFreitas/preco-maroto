package com.niceprice.product

import org.springframework.data.neo4j.annotation.NodeEntity;


@NodeEntity
class Category {

	String name

	String description

}
