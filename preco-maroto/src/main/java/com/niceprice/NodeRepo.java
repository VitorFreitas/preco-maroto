package com.niceprice;

import java.util.Set;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.niceprice.product.Product;
import com.niceprice.store.Store;

public interface NodeRepo extends GraphRepository<Product>{

	@Query("start store=node({0}) match store<--product return product")
	Set<Product> findByStore(Store store);
}
